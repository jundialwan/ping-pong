import { Machine } from './schema/Machine'
import rp from 'request-promise'

export default async function() {
  let machines = await Machine.find({}).exec()        
  
  machines.map((machine) => {
    rp.post(`http://${machine.ip}/ewallet/ping`, { form: {}, json: true, timeout: 2000 })  
    .then((result) => {
      machine.status = result.pong
      machine.save(() => {})
      console.log(result)
    })
    .catch(err => {
      console.log(err.message)
      machine.status = 0
      machine.save(() => {})
    })
  })
}