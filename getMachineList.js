import { Machine } from './schema/Machine'
import rp from 'request-promise'
import config from './config'

export default function() {
  rp.get(config.NODE_LIST_SERVICE, { json: true })    
    .then((nodes) => {
      nodes.forEach((e) => {
        let npm = e.npm

        Machine.findOne({ npm }).exec()
        .then((machine) => {        
          if (machine) {
            machine.ip = e.ip
            machine.save(() => {})
          } else {
            let newMachine = new Machine({...e, status: 0})
            newMachine.save(() => {})
          }        
        })    
        .catch(err => console.log(err))
      })
    }) 
    .then(() => console.log((new Date).toString(), 'getMachinesList success all'))   
    .catch(err => console.log(err))
}