import monggose, { Schema } from 'mongoose'

const MachineSchema = new Schema({
  ip: String,
  npm: String,  
  status: Number
}, { collection: 'Machine' })

const Machine = monggose.model('Machine', MachineSchema)

export { MachineSchema, Machine }