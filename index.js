import mongoose from 'mongoose'
import config from './config'

import getMachineList from './getMachineList'
import pingAll from './pingAll'

mongoose.connect(config.MONGO_URI, { useMongoClient: true })
mongoose.Promise = global.Promise

setInterval(getMachineList, 5000)
setInterval(pingAll, 7000)
